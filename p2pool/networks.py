from p2pool.bitcoin import networks
from p2pool.util import math

# CHAIN_LENGTH = number of shares back client keeps
# REAL_CHAIN_LENGTH = maximum number of shares back client uses to compute payout
# REAL_CHAIN_LENGTH must always be <= CHAIN_LENGTH
# REAL_CHAIN_LENGTH must be changed in sync with all other clients
# changes can be done by changing one, then the other

nets = dict(

    highfivecoin=math.Object(
        PARENT=networks.nets['highfivecoin'],
        SHARE_PERIOD=15,
        NEW_SHARE_PERIOD=15,
        CHAIN_LENGTH=24*60*60//10,
        REAL_CHAIN_LENGTH=24*60*60//10,
        TARGET_LOOKBEHIND=60,
        SPREAD=30,
        NEW_SPREAD=30,
        IDENTIFIER='83421ccf6822db88'.decode('hex'),
        PREFIX='44ad096bc6591e7e'.decode('hex'),
        P2P_PORT=7155,
        MIN_TARGET=4,
        MAX_TARGET=2**256//2**20 - 1,
        PERSIST=False,
        WORKER_PORT=7156,
        BOOTSTRAP_ADDRS='h5c.altmine.net h5c-p2pool.mine-coin.de'.split(' '),
        ANNOUNCE_CHANNEL='#p2pool-h5c',
        VERSION_CHECK=lambda v: True,
    ),

)
for net_name, net in nets.iteritems():
    net.NAME = net_name
